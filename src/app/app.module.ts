import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MaterialModule } from "./material.module";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { FilterBoxComponent } from "./shared/components/filter/filter-box/filter-box.component";
import { FilterStepComponent } from "./shared/components/filter/filter-step/filter-step.component";
import { DropdownSearchComponent } from "./shared/components/dropdown-search/dropdown-search.component";
import { DropdownSearchFieldComponent } from "./shared/components/dropdown-search/dropdown-search-field/dropdown-search-field.component";
import { DropdownOperatorComponent } from "./shared/components/dropdown-search/dropdown-operator/dropdown-operator.component";

import { StoreModule } from "@ngrx/store";
import { reducers, metaReducers } from "./reducers";
import { EventAttributeComponent } from "./shared/components/event/event-attribute/event-attribute.component";

@NgModule({
  declarations: [
    AppComponent,
    FilterBoxComponent,
    FilterStepComponent,
    DropdownSearchComponent,
    DropdownSearchFieldComponent,
    EventAttributeComponent,
    DropdownOperatorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    BrowserAnimationsModule,
    MaterialModule
  ],
  exports: [MaterialModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
