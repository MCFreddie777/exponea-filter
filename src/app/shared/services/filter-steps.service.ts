import { Injectable } from "@angular/core";
import { FilterStep } from "../models/FilterStep";
FilterStep;

@Injectable({
  providedIn: "root"
})
export class FilterStepsService {
  steps: FilterStep[] = [
    {
      id: 1,
      title: "session_end",
      properties: [
        {
          title: "price",
          operator: {
            id: 2,
            title: "in between",
            numOfValues: 2
          },
          inputValues: ["2", "5"]
        }
      ]
    }
  ];

  constructor() {}

  getSampleData(): FilterStep[] {
    return this.steps;
  }
}
