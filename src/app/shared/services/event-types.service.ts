import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class EventTypesService {
  eventTypes: Object[] = [
    { title: "python_script_event" },
    { title: "python_script_event2" },
    { title: "consent" },
    { title: "python_script_event3" },
    { title: "python_script_event4" },
    { title: "desync_test1" },
    { title: "desync_test2" },
    { title: "session_start" },
    { title: "session_end" },
    { title: "page_visit" },
    { title: "purchase" },
    { title: "old_a3" },
    { title: "B1" },
    { title: "B2" },
    { title: "B3" },
    { title: "C2" },
    { title: "C1" },
    { title: "C3" },
    { title: "view_item" }
  ];

  constructor() {}

  getEvents(): Object[] {
    return this.eventTypes;
  }
}
