import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class EventAttributesService {
  eventAttributes: object[] = [
    { id: 1, title: "price" },
    { id: 2, title: "index" },
    { id: 3, title: "number of items" },
    { id: 4, title: "timestamp" }
  ];

  constructor() {}

  getEvents(): object[] {
    return this.eventAttributes;
  }
}
