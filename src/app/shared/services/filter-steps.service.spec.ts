import { TestBed } from '@angular/core/testing';

import { FilterStepsService } from './filter-steps.service';

describe('FilterStepsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilterStepsService = TestBed.get(FilterStepsService);
    expect(service).toBeTruthy();
  });
});
