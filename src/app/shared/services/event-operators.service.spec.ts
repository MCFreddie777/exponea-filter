import { TestBed } from '@angular/core/testing';

import { EventOperatorsService } from './event-operators.service';

describe('EventOperatorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventOperatorsService = TestBed.get(EventOperatorsService);
    expect(service).toBeTruthy();
  });
});
