import { Injectable } from "@angular/core";
import { EventOperatorCategory } from "./../models/EventOperatorCategory";
import { EventOperator } from "../models/EventOperator";

@Injectable({
  providedIn: "root"
})
export class EventOperatorsService {
  operatorCategories: EventOperatorCategory[] = [
    {
      title: "String",
      operators: [
        { id: 1, title: "equal to", numOfValues: 1 },
        { id: 2, title: "in between", numOfValues: 2 },
        { id: 3, title: "less than", numOfValues: 1 },
        { id: 4, title: "greater than", numOfValues: 1 }
      ]
    },
    {
      title: "Number",
      operators: [
        { id: 5, title: "equals", numOfValues: 1 },
        { id: 6, title: "does not equal", numOfValues: 1 },
        { id: 7, title: "contains", numOfValues: 1 },
        { id: 8, title: "does not contain", numOfValues: 1 }
      ]
    }
  ];

  constructor() {}

  getOperators(): EventOperatorCategory[] {
    return this.operatorCategories;
  }
}
