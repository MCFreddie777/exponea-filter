import { TestBed } from '@angular/core/testing';

import { EventTypesService } from './event-types.service';

describe('EventTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventTypesService = TestBed.get(EventTypesService);
    expect(service).toBeTruthy();
  });
});
