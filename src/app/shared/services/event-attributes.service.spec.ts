import { TestBed } from '@angular/core/testing';

import { EventAttributesService } from './event-attributes.service';

describe('EventAttributesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EventAttributesService = TestBed.get(EventAttributesService);
    expect(service).toBeTruthy();
  });
});
