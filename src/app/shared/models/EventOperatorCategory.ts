import { EventOperator } from "./EventOperator";

export class EventOperatorCategory {
  title: string;
  operators: EventOperator[];
}
