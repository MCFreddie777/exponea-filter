import { EventProperty } from "./EventProperty";

export class FilterStep {
  id: number;
  title: string;
  properties: EventProperty[];
}
