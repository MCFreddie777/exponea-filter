import { EventOperator } from "./EventOperator";

export class EventProperty {
  constructor(title: string, operator: EventOperator, inputValues: string[]) {
    this.title = title;
    this.operator = operator;
    this.inputValues = inputValues;
  }

  title: string;
  operator: EventOperator;
  inputValues: string[];
}
