import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownOperatorComponent } from './dropdown-operator.component';

describe('DropdownOperatorComponent', () => {
  let component: DropdownOperatorComponent;
  let fixture: ComponentFixture<DropdownOperatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownOperatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownOperatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
