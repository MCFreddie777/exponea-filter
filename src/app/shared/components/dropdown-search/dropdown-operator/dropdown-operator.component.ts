import { Component, OnInit, Input } from "@angular/core";
import { EventProperty } from "src/app/shared/models/EventProperty";
import { EventOperator } from "src/app/shared/models/EventOperator";
import { EventOperatorCategory } from "./../../../models/EventOperatorCategory";
import { EventOperatorsService } from "src/app/shared/services/event-operators.service";

@Component({
  selector: "app-dropdown-operator",
  templateUrl: "./dropdown-operator.component.html",
  styleUrls: ["./dropdown-operator.component.scss"]
})
export class DropdownOperatorComponent implements OnInit {
  @Input() property: EventProperty;

  Arr = Array;
  operatorList: EventOperatorCategory[];

  constructor(private eventOperatorService: EventOperatorsService) {
    this.operatorList = this.eventOperatorService.getOperators();
  }

  ngOnInit() {}

  operatorExists(): boolean {
    return !(
      Object.entries(this.property.operator).length === 0 &&
      this.property.operator.constructor === EventOperator
    );
  }

  /* hotfix */
  select(id): void {
    let match;
    this.operatorList.forEach(category => {
      const search = category.operators.find(op => op.id === id);
      if (search) { match = search;    }
    });
    this.property.operator = { ...match };
  }
}
