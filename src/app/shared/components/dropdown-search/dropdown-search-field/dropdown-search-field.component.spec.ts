import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownSearchFieldComponent } from './dropdown-search-field.component';

describe('DropdownSearchFieldComponent', () => {
  let component: DropdownSearchFieldComponent;
  let fixture: ComponentFixture<DropdownSearchFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownSearchFieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownSearchFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
