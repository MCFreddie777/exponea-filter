import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-dropdown-search",
  templateUrl: "./dropdown-search.component.html",
  styleUrls: ["./dropdown-search.component.scss"]
})
export class DropdownSearchComponent implements OnInit {
  @Input() listItems: object[];
  @Input() itemType: string = "";
  @Input() selectedValue: any;

  constructor() {}

  ngOnInit() {}
}
