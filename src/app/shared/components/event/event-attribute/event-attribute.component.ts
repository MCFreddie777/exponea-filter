import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { EventProperty } from "../../../models/EventProperty";
import { EventAttributesService } from "./../../../services/event-attributes.service";

@Component({
  selector: "app-event-attribute",
  templateUrl: "./event-attribute.component.html",
  styleUrls: ["./event-attribute.component.scss"]
})
export class EventAttributeComponent implements OnInit {
  @Input() property: EventProperty;
  @Output() deleteAttribute: EventEmitter<any> = new EventEmitter();
  eventAttributesList: object[];

  constructor(private eventAttributesService: EventAttributesService) {}

  ngOnInit() {
    this.eventAttributesList = this.eventAttributesService.getEvents();
  }

  delete(property: EventProperty) {
    this.deleteAttribute.emit(property);
  }
}
