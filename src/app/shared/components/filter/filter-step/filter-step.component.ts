import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { FilterStep } from "../../../models/FilterStep";
import { EventTypesService } from "./../../../services/event-types.service";
import { EventProperty } from "./../../../models/EventProperty";
import { EventOperator } from "./../../../models/EventOperator";

@Component({
  selector: "app-filter-step",
  templateUrl: "./filter-step.component.html",
  styleUrls: ["./filter-step.component.scss"]
})
export class FilterStepComponent implements OnInit {
  @Input() step: FilterStep;

  @Output() deleteStep: EventEmitter<any> = new EventEmitter();
  @Output() duplicateStep: EventEmitter<any> = new EventEmitter();

  eventTypes: Object[];

  constructor(private eventTypeService: EventTypesService) {}

  ngOnInit() {
    this.eventTypes = this.eventTypeService.getEvents();
  }

  delete(step: FilterStep): void {
    this.deleteStep.emit(step);
  }

  duplicate(step: FilterStep): void {
    this.duplicateStep.emit(step);
  }

  addAttribute(step: FilterStep): void {
    this.step.properties.push(new EventProperty(null, new EventOperator(), []));
  }

  deleteAttribute(property: EventProperty): void {
    this.step.properties = this.step.properties.filter(
      item => item !== property
    );
  }
}
