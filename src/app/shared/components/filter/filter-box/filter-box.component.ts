import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { cloneDeep } from "lodash";

import { FilterStep } from "../../../models/FilterStep";
import { FilterStepsService } from "src/app/shared/services/filter-steps.service";

@Component({
  selector: "app-filter-box",
  templateUrl: "./filter-box.component.html",
  styleUrls: ["./filter-box.component.scss"]
})
export class FilterBoxComponent implements OnInit {
  steps: FilterStep[] = [];

  constructor(private filterStepsService: FilterStepsService) {}

  ngOnInit() {
    // this.steps = this.filterStepsService.getSampleData();
  }

  newStep(): void {
    this.steps.push({
      id: this.steps.length + 1,
      title: "Unnamed step",
      properties: []
    });
  }

  /*
   * Deletes the filterStep and remaps ids
   */
  deleteStep(step: FilterStep): void {
    this.steps = this.steps
      .filter(item => step.id !== item.id)
      .map((element, index) => ({
        ...element,
        id: index + 1
      }));
  }

  duplicateStep(step: FilterStep): void {
    // insted of push i went for immutable array
    this.steps = [
      ...this.steps,
      {
        id: this.steps.length + 1,
        title: step.title,
        properties: cloneDeep(step.properties)
      }
    ];
  }

  clearFilter(): void {
    this.steps = [];
  }

  logData(): void {
    console.log(this.steps);
  }
}
